---
layout: markdown_page
title: Product categories
---

## Introduction

We want intuitive interfaces both within the company and with the wider community.
This makes it more efficient for everyone to contribute or to get a question answered.
Therefore, the following interfaces are based on the product categories defined on this page:

- [Product Vision](/direction/product-vision/)
- [Direction](/direction/#functional-areas)
- [Software Development Life-Cycle (SDLC)](/sdlc/#stacks)
- [Product Features](/features/)
- [Documentation](https://docs.gitlab.com/ee/#complete-devops-with-gitlab)
- [Backend teams](/handbook/backend/)
- [Product manager responsibilities](/handbook/product/#who-to-talk-to-for-what)
- Our deck, the slides that we use to describe the company
- Product marketing specializations

## Definitions

The functionality of GitLab is ordered in 4 level hierarchy:

1. Stages: One of the 7 DevOps lifecycle stages (see below), one of the two areas that span the entire lifecycle, or a non-lifecycle stage like Auth or BizOps.
1. Categories: High-level capabilities that may be a standalone product at another company. e.g. Portfolio Management.
1. Capabilities: An optional group of functionality that is made up of smaller discrete features. e.g. Epics. Capabilities are listed as the second tier of bullets below. We have [feature categories](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/feature_categories.yml) but it is incomplete and not used everywhere.
1. Features: Small, discrete functionalities. e.g. Issue weights. Some common features are listed within parentheses to facilitate finding responsible PMs by keyword. Features are maintained in [features.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml).

## DevOps lifecycle stages

![DevOps lifecycle](/handbook/product/categories/devops-loop-and-spans.png)

## DevOps Stage Definitions
The stages of the DevOps lifecycle are defined as:

**Manage**  
Organizations manage their operation to optimize their value to their users, customers, and stakeholders by continuously improving their products and services. As such, the business needs insight and data into their effectiveness and they need specific process, cycle, and financial metrics to improve their performance. The process of managing the business is always on.

**Plan**  
Planning is focused on the prioritization of ideas, allocation of resources ($, human and technology), scheduling projects, tracking status, coordination across the business, and resolving conflicts between efforts. Typically planning includes practices such as: Collaboration, Issue Management, Project Management, Program Management, Portfolio Management, Resource Management, Requirements Management, and Financial Management (as it relates to projects).

**Create**  
Collaboratively design, develop, and implement capabilities in the application to meet business goals and objectives. Teams establish processes and mechanisms to track and review multiple versions of their code as they iterate through frequent improvements.

**Verify**  
Verification ensures the quality of the software by automatically and consistently building, integrating, and testing code changes; minimizing conflicts and rework. Verification includes functional testing (unit, integration, and acceptance) and non-functional testing (performance, security, and usability). Ideally, verification tests are fully automated and provide rapid feedback so that only high quality changes are accepted.

**Package**  
Assemble and manage the different versions of components, libraries, and other elements that are required to run the application, so that the application can be deployed consistently and repeatedly to different execution environments.

**Release**  
Deliver and deploy the application in the target environment. When 'releasing' the application, the environment is typically production. Often teams will use strategies to gradually release changes into their environment, such as Canary deployments or Blue/Green deployments.

**Configure**  
Make application-specific settings that enable the application to be fully functional and optimized in a specific environment. Ideally configuration settings and details are centrally managed (like code) and are applied automatically to reduce the potential for human error.

**Monitor**  
Rapid and reliable feedback from production is critical in order to embrace continuous improvement. This feedback must be collected from both external and internal vantage points to be effective. Applications must be designed and built to allow for telemetry and utilization data to enable the business and delivery team to iterate and continuously improve the application.

**Secure**  
Ensuring the application is secure and trusted is an activity that spans the entire development lifecycle from planning to monitoring. Requirements and policies should set security expectations, design and development incorporate secure practices, every change should be tested for security (SAST,DAST), libraries and dependencies tracked and managed, containers and infrastructure secured, and the entire lifecycle monitored to ensure compliance. Delivering secure applications depends on every stage in the life cycle.

## GitLab Product Organization
At GitLab the Dev and Ops split is different because our CI/CD functionality is one codebase that falls under Ops.

## Dev

- Product: [Job]
- Backend: [Tommy]
- Product Marketing: [John]

1. Manage - [Jeremy] and [Victor]
    - Reporting & Analytics
        - Cycle Analytics - [Jeremy]
        - DevOps Score (previously Conversational Development Index / ConvDev Index) - [Jeremy]
        - [Usage statistics](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) (Version check (incl. version.gitlab.com), Usage ping) - [Victor]
    - Value Stream Management - [Victor] <kbd>Planned 2019</kbd>
    - Financial Management <kbd>Planned 2019</kbd>
    - Audit Management <kbd>Planned 2019</kbd>
1. Plan - [Victor]
    - Project management
        - Issue and merge request trackers (assignees, milestones, time tracking, due dates, labels, issue weights, quick actions, email notifications, todos, search, Elasticsearch integration, Jira and other third-party issue management integration)
        - Issue page and merge request page commenting and discussions
        - Issue boards
    - Program Management <kbd>Planned 2019</kbd>
    - Portfolio Management <kbd>New in 2018</kbd>
        - Epics
        - Roadmaps
    - Requirements Management <kbd>Planned 2019</kbd>
    - [Service desk]
    - Chat integration (Mattermost, Slack)
    - Markdown
1. Create - [James] and [Andreas]
    - Source code management - [James]
        - Version control (Git repository, Commits, file locking, LFS, protected branches, mirroring, housekeeping (e.g. `git gc`), hooks)
        - Gitaly
    - Projects (Project creation, project templates, project import/export, importers) - [Andreas]
    - Code Review (merge requests, diffs, approvals) - [James]
    - Web IDE - [James] <kbd>New in 2018</kbd>
    - Wiki - [Andreas]
    - Snippets - [Andreas]
    - Admin Area - [Andreas]
1. Administration - [Jeremy]
    - User management & authentication (incl. LDAP, signup, abuse)
    - Groups and [Subgroups]
    - Navigation
    - Audit log
    - GitLab.com (our hosted offering of GitLab)
    - Subscriptions (incl. license.gitlab.com and customers.gitlab.com)
    - [Internationalization](https://docs.gitlab.com/ee/development/i18n/)
1. Gitter - n/a
1. Production - [Job]
1. [Geo] - [Andreas]

## Ops

- Product: [Mark]
- Backend: [Dalia]
- Product Marketing: [William]

1. Verify - [Fabio]
    - [Continuous Integration (CI)]
        - Unit Testing
        - Integration Testing
        - System Testing <kbd>Planned 2019</kbd>
        - Acceptance Testing
        - Performance Testing <kbd>New in 2018</kbd>
        - Usability Testing <kbd>Planned 2019</kbd>
        - Compatibility Testing <kbd>Planned 2019</kbd>
        - GitLab Runner
1. Package - [Josh]
    - Container Registry
    - Binary Repository <kbd>Planned 2018</kbd>
1. Release - [Fabio]
    - [Continuous Delivery (CD)]
        - Review apps
    - Release Automation
    - [Pages]
1. Configure - [Daniel]
    - Application Control Panel <kbd>Planned 2018</kbd>
        - Auto DevOps
    - Infrastructure Configuration <kbd>New in 2018</kbd>
    - Operations <kbd>Planned 2018</kbd>
        - ChatOps <kbd>New in 2018</kbd>
    - Feature Management <kbd>Planned 2018</kbd>
        - Feature flags
    - Serverless <kbd>Planned 2019</kbd>
1. Monitor - [Josh]
    - Application Performance Monitoring (APM)
        - Metrics
        - Tracing <kbd>Planned 2018</kbd>
    - Infrastructure Monitoring <kbd>New in 2018</kbd>
        - Cluster Monitoring <kbd>New in 2018</kbd>
    - Production Monitoring <kbd>Planned 2018</kbd>
    - Error Tracking <kbd>Planned 2018</kbd>
    - Logging <kbd>Planned 2018</kbd>
    - Incident Management <kbd>Planned 2019</kbd>
1. Secure - [Fabio]
    - Security Testing
        - Static Application Security Testing (SAST) <kbd>New in 2018</kbd>
        - Dynamic Application Security Testing (DAST) <kbd>New in 2018</kbd>
        - Dependency Scanning <kbd>New in 2018</kbd>
        - Container Scanning <kbd>New in 2018</kbd>
        - Runtime Application Self-Protection (RASP) <kbd>Planned 2018</kbd>
    - License Management <kbd>Planned 2018</kbd>
1. Distribution - [Josh] (*note: the engineering backend team reports into the dev backend department*)
    - Omnibus
    - Cloud Native Installation <kbd>Planned 2018</kbd>
1. Meltano (née BizOps) - [Josh]

## Composed categories

GitLab also does the things below that are composed of multiple categories.

1. Software Composition Analysis (SCA) = Dependency Scanning + License Management
1. Interactive Application Security Testing (IAST) = Dynamic application security testing (DAST) + Runtime Application Self-Protection (RASP)

[Jeremy]: /team/#d3arWatson
[Fabio]: /team/#bikebilly
[Josh]: /team/#joshlambert
[Mark]: /team/#MarkPundsack
[William]: /team/#thewilliamchia
[James]: /team/#jamesramsay
[Job]: /team/#Jobvo
[John]: /team/#j_jeremiah
[Victor]: /team/#victorwu416
[Daniel]: /team/#danielgruesso
[Tommy]: /team/#tommy.morgan
[Dalia]: /team/#dhavens
[Andreas]: /team/#andreasmarc
[Pages]: /features/pages/
[Geo]: /features/gitlab-geo/
[Continuous Integration (CI)]: /features/gitlab-ci-cd/
[Continuous Delivery (CD)]: /features/gitlab-ci-cd/
[Subgroups]: /features/subgroups/
[Service Desk]: /features/service-desk/
