---
layout: markdown_page
title: Support Engineering Handbook
---

## Welcome to the GitLab Support Handbook
{: .no_toc}


----

### On this page
{:.no_toc}

- TOC
{:toc}

## Support Engineering At GitLab

We are on the vanguard of helping our customers, from single-instance Omnibus deployments to large 30 Node High Availability set ups. This variety means you will always be on your toes working with technologies ranging from AJAX request parsing, Docker, Linux file permissions, Rails, and many more. Due to this extreme variablity, it's core that we try and keep our processes as lean as possible.

## Our Processes

There are three core tennants that we should keep in mind that articulate our responsiblities:

- [How to Prioritize Tickets](/handbook/support/support-engineering/prioritizing-tickets.html)
- How to Submit issues to Product/Development
- How to Submit Code to the GitLab Application

The above are links to the appropriate sections of our handbook which further outline how each work.

## How We're Doing

The [Zendesk Insights dashboard](https://gitlab.Zendesk.com/agent/reporting/analytics/period:0/dashboard:buLJ3T7IiFnr) lists the activity for all of our current channels and summarizes the last 30 days (Zendesk login required).

## Our Meetings

Support has 3 meetings a week. Tuesday APAC Support call which will cover Metrics/Demos/Open format questions. Tuesday AMER call, which is focused on Demos and solving challenging tickets as a group. Lastly, the Friday AMER call which is focused on metrics/open format questions. This is how we coordinate and help us all grow together.
